
To Run:
1, Run the java classes:
Add lib/gson-2.3.1.jar in classpath
Compile com/sort/*.java
a, JavaSource\com\sort\Sorter.java
b, JavaSource\com\sort\WordCount.java

2, Run the node app:
npm install
node bin/www

3, Access the application from browser:
http://localhost:3000/


Tools : Node, Express, Jade, AngularJS, Java


Design:
The UI allows the user to select one or more sort algorithms.
Number of trials is used to find the average execution time.
Total word count and unique word count are also displayed along with the sort result.

AngularJS http service is used to communicate with the NodeJS.
NodeJS interacts with the Java code through sockets (ports 3200 & 3201).
Following algorithms are implemented in Java :
	Bubble, Selection, Insertion, Merge & Quick
