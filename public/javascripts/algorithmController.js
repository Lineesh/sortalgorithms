var algorithmApp = angular.module('algorithmApp', ['ngResource']);

algorithmApp.service('PerformSort', function($resource) {
  return $resource('/performSort/:id');
});

algorithmApp.service('WordCount', function($resource) {
  return $resource('/wordCount/:id');
});


algorithmApp.factory('SortAlgorithmFactory', function($http, PerformSort, WordCount) {
	
	var factory = {};
	
	factory.performBubbleSort = function(sortInput, dataType, eliminateDuplicate, numberOfTrials) {
		var paramToServer = factory.buildSortRequest('bubble', sortInput, dataType, false, eliminateDuplicate, numberOfTrials);
		
		$http.post('/performSort', paramToServer)
        .success(function (data2, status, headers, config) {
			document.getElementById("sortOutput").value = data2.sortOutput;
        })
        .error(function(data, status, headers, config){
			console.log("An error occured.data=" + data);
        });
	};
	
	factory.findWordCount = function(sortInput, dataType) {
		var paramToServer = '{"sortInput":"' + sortInput + '", "dataType":"' + dataType + '"}';
		return $http.post('/wordCount', paramToServer);
	}

	factory.findSortTime = function(sortInput, dataType, algorithm, eliminateDuplicate, numberOfTrials) {
		var paramToServer = factory.buildSortRequest(algorithm, sortInput, dataType, true, eliminateDuplicate, numberOfTrials);
		return $http.post('/performSort', paramToServer);
	};
	
	factory.buildSortRequest = function(sortType, sortInput, dataType, timeOnly, eliminateDuplicate, numberOfTrials) {
		var sortRequest = {
			"sortType" : sortType,
			"sortInput" : sortInput,
			"dataType" : dataType,
			"timeOnly" : timeOnly,
			"eliminateDuplicate" : eliminateDuplicate,
			"numberOfTrials" : numberOfTrials
		};
		return JSON.stringify(sortRequest);
	};
	
	return factory;

});

algorithmApp.controller('AlgorithmController', function($scope, SortAlgorithmFactory) {
	$scope.typeOfAlgorithm = {
		'bubble' : true,
		'selection' : true,
		'insertion' : true,
		'merge' : true,
		'quick' : true
	};

	$scope.sortTimings = {
      'bubble' : 0,
	  'bubbleDone' : '',
	  'selection' : 0,
	  'selectionDone' : '',
	  'insertion' : 0,
	  'insertionDone' : '',
	  'merge' : 0,
	  'mergeDone' : '',
	  'quick' : 0,
	  'quickDone' : ''
  };
    
  $scope.dataType = 'String';

  $scope.eliminateDuplicate = 'true';

  $scope.sortInput;
  $scope.sortOutput;
  $scope.totalWordCount;
  $scope.uniqueWordCount;
  $scope.numberOfTrials = '10';
	
  $scope.resetSort = function() {
	  $scope.sortTimings.bubble = 0;
	  $scope.sortTimings.selection = 0;
	  $scope.sortTimings.insertion = 0;
	  $scope.sortTimings.merge = 0;
	  $scope.sortTimings.quick = 0; 
	  $scope.sortTimings.bubbleDone = '';
	  $scope.sortTimings.selectionDone = '';
	  $scope.sortTimings.insertionDone = '';
	  $scope.sortTimings.mergeDone = '';
	  $scope.sortTimings.quickDone = '';
	  $scope.sortOutput = ''; 
	  $scope.totalWordCount = '';
	  $scope.uniqueWordCount = '';
  }
	
  $scope.performSort = function() {
	  
	  SortAlgorithmFactory.performBubbleSort($scope.sortInput, $scope.dataType, $scope.eliminateDuplicate, $scope.numberOfTrials);
	  
	  $scope.findWordCount($scope.sortInput, $scope.dataType);
	  
	  if ($scope.typeOfAlgorithm.bubble) {
		  $scope.findSortTime('bubble');
	  } 
	  if ($scope.typeOfAlgorithm.selection) {
		  $scope.findSortTime('selection');
	  } 
	  if ($scope.typeOfAlgorithm.insertion) {
		  $scope.findSortTime('insertion');
	  } 
	  if ($scope.typeOfAlgorithm.merge) {
		  $scope.findSortTime('merge');
	  } 
	  if ($scope.typeOfAlgorithm.quick) {
		  $scope.findSortTime('quick');
	  }
	   
	  
  };
	
	$scope.findWordCount = function() {
		var promise = SortAlgorithmFactory.findWordCount($scope.sortInput, $scope.dataType);
		promise.then(
			  function(payload) { 
				  $scope.totalWordCount = payload.data.totalWordCount;
				  $scope.uniqueWordCount = payload.data.uniqueWordCount;
			  },
			  function(errorPayload) {
				  $log.error('Error finding sort time', errorPayload);
			  }
		);
	}

	$scope.findSortTime = function(algorithm) {
		  var promise = SortAlgorithmFactory.findSortTime($scope.sortInput, $scope.dataType, algorithm, $scope.eliminateDuplicate, $scope.numberOfTrials);
		  promise.then(
			  function(payload) { 
				  $scope.sortTimings[algorithm] = payload.data.sortOutput;
				  $scope.sortTimings[algorithm + 'Done'] = 'Done';
			  },
			  function(errorPayload) {
				  $log.error('Error finding sort time', errorPayload);
			  }
		  );
	};
	
});

