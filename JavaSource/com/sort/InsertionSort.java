package com.sort;

import java.util.ArrayList;
import java.util.List;

public class InsertionSort<T extends Comparable> {
	
	public static void main(String s[]) {
		InsertionSort is = new InsertionSort<Integer>();
		List<Integer> ip = new ArrayList();
		ip.add(1);
		ip.add(50);
		ip.add(23);
		ip.add(56);
		ip.add(12);
		ip.add(9);
		ip.add(10);
		System.out.println(ip);
		List<Integer> op = is.sort(ip);
		System.out.println(op);
	}
	
	public List<T> sort(List<T> input) {
		for (int i = 1; i < input.size(); i++) {
			int j = i - 1;
			while((j >= 0) && (input.get(j).compareTo(input.get(i)) > 0)) {
				j--;
			}
			if (i != j+1) {
				T swap = input.get(i);
				input.remove(i);
				input.add(j+1, swap);
			}
		}
		return input;
	}
	

}
