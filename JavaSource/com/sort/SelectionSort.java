package com.sort;

import java.util.ArrayList;
import java.util.List;

public class SelectionSort<T extends Comparable> {
	
	public static void main(String s[]) {
		SelectionSort ss = new SelectionSort<Integer>();
		List<Integer> ip = new ArrayList();
		ip.add(1);
		ip.add(50);
		ip.add(12);
		ip.add(23);
		ip.add(56);
		ip.add(12);
		ip.add(9);
		ip.add(10);
		ip.add(12);
		System.out.println(ip);
		List<Integer> op = ss.sort(ip);
		System.out.println(op);

	}
	
	public List<T> sort(List<T> input) {
		for (int i = 0; i < input.size(); i++) {
			T currVal = input.get(i);
			int swapIndex = i;
			T swapValue = currVal;
			for (int j = i+1; j < input.size(); j++) {
				if (input.get(j).compareTo(swapValue) < 0) {
					swapValue = input.get(j);
					swapIndex = j;
				}
			}
			
			input.set(i, swapValue);
			input.set(swapIndex, currVal);
		}
		return input;
	}

}
