package com.sort;

import java.util.ArrayList;
import java.util.List;

public class QuickSort<T extends Comparable> {

	public static void main(String s[]) {
		QuickSort qs = new QuickSort<Integer>();
		List<Integer> ip = new ArrayList();
		ip.add(1);
		ip.add(50);
		ip.add(23);
		ip.add(51);
		ip.add(56);
		ip.add(12);
		ip.add(9);
		ip.add(10);
		System.out.println(ip);
		qs.sort(ip, 0, ip.size() - 1);
		System.out.println(ip);
	}

	public void sort(List<T> array, int start, int end) {
		int i = start;
		int k = end;

		if (end - start >= 1) {
			T pivot = array.get(start);

			while (k > i) {
				while (array.get(i).compareTo(pivot) <= 0 && i <= end && k > i)
					i++; 
				while (array.get(k).compareTo(pivot) > 0 && k >= start && k >= i)
					k--;
				if (k > i)
					swap(array, i, k);
			}
			swap(array, start, k);
			sort(array, start, k - 1);
			sort(array, k + 1, end);
		} else {
			return; 
		}
	}

	private void swap(List<T> input, int i, int j) {
		//System.out.println("Swapping " + i + " and " + j);
		T temp = input.get(i);
		input.set(i, input.get(j));
		input.set(j, temp);
		//System.out.println("		" + input);
	}

}
