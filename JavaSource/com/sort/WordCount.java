package com.sort;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class WordCount {

	public static void main(String s[]) throws Exception {
		ServerSocket serverSocket = new ServerSocket(3201);

		while (true) {
			Socket clientSocket = serverSocket.accept();

			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),
					true);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));

			String message = in.readLine();
			Gson gson = new Gson();
			WordCountInput sortInput = gson.fromJson(message,
					WordCountInput.class);
			WordCount wordCount = new WordCount();
			WordCountOutput output = wordCount.getTotalWordCount(sortInput);
			String returnMessage = gson.toJson(output);
			out.print(returnMessage);
			out.flush();
			out.close();
		}
	}

	private WordCountOutput getTotalWordCount(WordCountInput input) {
		WordCountOutput output = new WordCountOutput();
		if (input.getDataType().equals("Integer")) {
			List<Integer> integerList = buildIntegerList(input.getSortInput());
			BubbleSort<Integer> bs = new BubbleSort<Integer>();
			List<Integer> sortedList = bs.sort(integerList);
			output.setTotalWordCount(sortedList.size());
			List uniqueIntegers = eliminateDuplicate(sortedList);
			output.setUniqueWordCount(uniqueIntegers.size());
		} else if (input.getDataType().equals("String")) {
			List<String> stringList = buildStringList(input.getSortInput());
			BubbleSort<String> bs = new BubbleSort<String>();
			List<String> sortedList = bs.sort(stringList);
			output.setTotalWordCount(sortedList.size());
			List uniqueStrings = eliminateDuplicate(sortedList);
			output.setUniqueWordCount(uniqueStrings.size());
		}
		return output;
	}

	private List eliminateDuplicate(List input) {
		List output = new ArrayList();
		for (int i = 0; i < input.size()-1; i++) {
			if (!input.get(i).equals(input.get(i+1))) {
				output.add(input.get(i));
			}
		}
		output.add(input.get(input.size()-1));
		return output;
	}

	private List<String> buildStringList(String input) {
		List<String> dataToSort = new ArrayList();
		String[] inputArray = input.split(" ");
		for (String ip : inputArray) {
			dataToSort.add(ip);
		}
		return dataToSort;
	}

	private List<Integer> buildIntegerList(String input) {
		List<Integer> dataToSort = new ArrayList();
		String[] inputArray = input.split(" ");
		for (String ip : inputArray) {
			dataToSort.add(new Integer(ip));
		}
		return dataToSort;
	}

	static class WordCountOutput {
		private int totalWordCount;
		private int uniqueWordCount;
		
		public int getTotalWordCount() {
			return totalWordCount;
		}
		public void setTotalWordCount(int totalWordCount) {
			this.totalWordCount = totalWordCount;
		}
		public int getUniqueWordCount() {
			return uniqueWordCount;
		}
		public void setUniqueWordCount(int uniqueWordCount) {
			this.uniqueWordCount = uniqueWordCount;
		}
	}
	
	static class WordCountInput {
		private String sortInput;
		private String dataType;

		public String getSortInput() {
			return sortInput;
		}

		public void setSortInput(String sortInput) {
			this.sortInput = sortInput;
		}

		public String getDataType() {
			return dataType;
		}

		public void setDataType(String dataType) {
			this.dataType = dataType;
		}
	}
}
