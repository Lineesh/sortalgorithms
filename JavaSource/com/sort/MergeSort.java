package com.sort;

import java.util.ArrayList;
import java.util.List;

public class MergeSort<T> {
	
	public static void main(String s[]) {
		MergeSort ms = new MergeSort();
		List<Integer> ip = new ArrayList();
		ip.add(9);
		ip.add(5);
		ip.add(2);
		ip.add(6);
		ip.add(1);
		ip.add(3);
		ip.add(4);
		System.out.println(ip);
		List<Integer> op = ms.sort(ip);
		
		System.out.println(op);
	}
	
	
	
	public List sort(List<T> ip) {
		if (ip.size() == 1) {
			return ip;
		}
		
		List<T> subLists = split(ip);
		List<T> l1 = sort((List)subLists.get(0));
		List<T> l2 = sort((List)subLists.get(1));
		
		int i1 = 0, i2 = 0;
		List<T> op = new ArrayList();
		for (int i = 0; i < l1.size() + l2.size(); i++) {
			if (i1 == l1.size()) {
				return copy(op, i2, l2);
			} else if (i2 == l2.size()) {
				return copy(op, i1, l1);
			}
			if (l1.get(i1).toString().compareTo(l2.get(i2).toString()) < 0) {
				op.add(l1.get(i1));
				i1++;
			} else {
				op.add(l2.get(i2));
				i2++;
			}
		}
		
		return op;
	}
	
	private List<T> copy(List<T> op, int index, List<T> ip) {
		for (int i = index; i < ip.size(); i++) {
			op.add(ip.get(i));
		}
		return op;
	}
	
	private List split(List<T> ip) {
		List op = new ArrayList();
		op.add(ip.subList(0, ip.size()/2));
		op.add(ip.subList(ip.size()/2, ip.size()));
		return op;
	}

}
