package com.sort;

import java.util.ArrayList;
import java.util.List;

public class BubbleSort<T extends Comparable> {
	
	public static void main(String s[]) {
		
		BubbleSort bs = new BubbleSort<Integer>();
		List<Integer> ip = new ArrayList();
		ip.add(1);
		ip.add(50);
		ip.add(23);
		ip.add(56);
		ip.add(12);
		ip.add(9);
		ip.add(10);
		System.out.println(ip);
		List<Integer> op = bs.sort(ip);
		System.out.println(op);
		
		/*
		BubbleSort bs = new BubbleSort<String>();
		List<String> ip = new ArrayList();
		ip.add("zzz");
		ip.add("aaa");
		ip.add("ggg");
		ip.add("ccc");
		ip.add("yyy");
		System.out.println(ip);
		List<String> op = bs.sort(ip);
		System.out.println(op);
		*/
	}
	
	public List<T> sort(List<T> ip) {
		for (int j = 0; j < ip.size(); j++) {
			for (int i = 0; i < ip.size()-1; i++) {
				if (((Comparable)ip.get(i)).compareTo(ip.get(i+1)) > 0) {
					T temp = ip.get(i);
					ip.set(i, ip.get(i+1));
					ip.set(i+1, temp);
				}
			}
		}
		return ip;
	}

}
