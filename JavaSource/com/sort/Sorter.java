package com.sort;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class Sorter {
	
	public static void main(String s[]) throws Exception {
		ServerSocket serverSocket = new ServerSocket(3200);
		
		while (true) {
			Socket clientSocket = serverSocket.accept();
			
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
			
			String message = in.readLine();
			Gson gson = new Gson();
			SortInput sortInput = gson.fromJson(message, SortInput.class);
			
			Sorter sorter = new Sorter();

			if (!sortInput.getTimeOnly()) {
				//System.out.println("received sort request");
				String sortedString = sorter.bubbleSort(sortInput.getSortInput(), sortInput.getDataType(), sortInput.isEliminateDuplicate());
				sortedString = sortedString.substring(1);
				sortedString = sortedString.substring(0, sortedString.length() - 1);
				out.print(sortedString);
			} else {
				//System.out.println("received timeonly request");
				long time = sorter.findSortTime(sortInput.getSortInput(), sortInput.getDataType(), sortInput.getAlgorithm(), sortInput.getNumberOfTrials());
				//System.out.println("Client says.time:" + time);
				out.print(time);
			}
			out.flush();
			out.close();
		}
	}
	
	private long findSortTime(String input, String dataType, String algorithm, int numberOfTrials) {
		if (algorithm.equals("bubble")) {
			return findBubbleSortTime(input, dataType, numberOfTrials);
		} else if (algorithm.equals("merge")) {
			return findMergeSortTime(input, dataType, numberOfTrials);
		} else if (algorithm.equals("selection")) {
			return findSelectionSortTime(input, dataType, numberOfTrials);
		} else if (algorithm.equals("insertion")) {
			return findInsertionSortTime(input, dataType, numberOfTrials);
		} else if (algorithm.equals("quick")) {
			return findQuickSortTime(input, dataType, numberOfTrials);
		} else {
			return 99999;
		}
		
	}

	private long findQuickSortTime(String input, String dataType, int numberOfTrials) {
		long startTime = System.currentTimeMillis();
		if (dataType.equals("Integer")) {
			QuickSort<Integer> bs = new QuickSort<Integer>();
			List<Integer> integerList = buildIntegerList(input);
			for (int i = 0; i < numberOfTrials; i++) {
				bs.sort(integerList, 0, integerList.size()-1);
			}
			return ((System.currentTimeMillis() - startTime) / numberOfTrials);
		} else if (dataType.equals("String")) {
			QuickSort<String> bs = new QuickSort<String>();
			List<String> stringList = buildStringList(input);
			for (int i = 0; i < numberOfTrials; i++) {
				bs.sort(stringList, 0, stringList.size()-1);
			}
			return ((System.currentTimeMillis() - startTime) / numberOfTrials);
		} else {
			return 999990;
		}
	}

	private long findInsertionSortTime(String input, String dataType, int numberOfTrials) {
		long startTime = System.currentTimeMillis();
		if (dataType.equals("Integer")) {
			InsertionSort<Integer> bs = new InsertionSort<Integer>();
			List<Integer> integerList = buildIntegerList(input);
			for (int i = 0; i < numberOfTrials; i++) {
				List<Integer> sortedList = bs.sort(integerList);
			}
			return ((System.currentTimeMillis() - startTime) / numberOfTrials);
		} else if (dataType.equals("String")) {
			SelectionSort<String> bs = new SelectionSort<String>();
			List<String> stringList = buildStringList(input);
			for (int i = 0; i < numberOfTrials; i++) {
				List<String> sortedList = bs.sort(stringList);
			}
			return ((System.currentTimeMillis() - startTime) / numberOfTrials);
		} else {
			return 999990;
		}
	}

	private long findSelectionSortTime(String input, String dataType, int numberOfTrials) {
		long startTime = System.currentTimeMillis();
		if (dataType.equals("Integer")) {
			SelectionSort<Integer> bs = new SelectionSort<Integer>();
			List<Integer> integerList = buildIntegerList(input);
			for (int i = 0; i < numberOfTrials; i++) {
				List<Integer> sortedList = bs.sort(integerList);
			}
			return ((System.currentTimeMillis() - startTime) / numberOfTrials);
		} else if (dataType.equals("String")) {
			SelectionSort<String> bs = new SelectionSort<String>();
			List<String> stringList = buildStringList(input);
			for (int i = 0; i < numberOfTrials; i++) {
				List<String> sortedList = bs.sort(stringList);
			}
			return ((System.currentTimeMillis() - startTime) / numberOfTrials);
		} else {
			return 999990;
		}
	}

	private long findBubbleSortTime(String input, String dataType, int numberOfTrials) {
		long startTime = System.currentTimeMillis();
		if (dataType.equals("Integer")) {
			BubbleSort<Integer> bs = new BubbleSort<Integer>();
			List<Integer> integerList = buildIntegerList(input);
			for (int i = 0; i < numberOfTrials; i++) {
				List<Integer> sortedList = bs.sort(integerList);
			}
			return ((System.currentTimeMillis() - startTime) / numberOfTrials);
		} else if (dataType.equals("String")) {
			BubbleSort<String> bs = new BubbleSort<String>();
			List<String> stringList = buildStringList(input);
			for (int i = 0; i < numberOfTrials; i++) {
				List<String> sortedList = bs.sort(stringList);
			}
			return ((System.currentTimeMillis() - startTime) / numberOfTrials);
		} else {
			return 999990;
		}
	}

	private long findMergeSortTime(String input, String dataType, int numberOfTrials) {
		long startTime = System.currentTimeMillis();
		if (dataType.equals("Integer")) {
			MergeSort<Integer> bs = new MergeSort<Integer>();
			List<Integer> integerList = buildIntegerList(input);
			for (int i = 0; i < numberOfTrials; i++) {
				List<Integer> sortedList = bs.sort(integerList);
			}
			return ((System.currentTimeMillis() - startTime) / numberOfTrials);
		} else if (dataType.equals("String")) {
			MergeSort<String> bs = new MergeSort<String>();
			List<String> stringList = buildStringList(input);
			for (int i = 0; i < numberOfTrials; i++) {
				List<String> sortedList = bs.sort(stringList);
			}
			return ((System.currentTimeMillis() - startTime) / numberOfTrials);
		} else {
			return 999991;
		}
	}

	private String bubbleSort(String input, String dataType, boolean eliminateDuplicate) {
		if (dataType.equals("Integer")) {
			BubbleSort<Integer> bs = new BubbleSort<Integer>();
			List<Integer> sortedList = bs.sort(buildIntegerList(input));
			if (eliminateDuplicate) {
				return eliminateDuplicate(sortedList).toString();
			} else {
				return sortedList.toString();
			}
		} else if (dataType.equals("String")) {
			BubbleSort<String> bs = new BubbleSort<String>();
			List<String> sortedList = bs.sort(buildStringList(input));
			if (eliminateDuplicate) {
				return eliminateDuplicate(sortedList).toString();
			} else {
				return sortedList.toString();
			}
		} else {
			return "";
		}
	}
	
	private List<String> eliminateDuplicate(List input) {
		List output = new ArrayList();
		for (int i = 0; i < input.size()-1; i++) {
			if (!input.get(i).equals(input.get(i+1))) {
				output.add(input.get(i));
			}
		}
		output.add(input.get(input.size()-1));
		return output;
	}
	
	private List<String> buildStringList(String input) {
		List<String> dataToSort = new ArrayList();
		String[] inputArray = input.split(" ");
		for (String ip : inputArray) {
			dataToSort.add(ip);
		}
		return dataToSort;
	}
	
	private List<Integer> buildIntegerList(String input) {
		List<Integer> dataToSort = new ArrayList();
		String[] inputArray = input.split(" ");
		for (String ip : inputArray) {
			dataToSort.add(new Integer(ip));
		}
		return dataToSort;
	}

	
	private long findMergeSortTime(List<Integer> ip) {
		MergeSort<Integer> ms = new MergeSort();
		long t1 = System.currentTimeMillis();
		ms.sort(ip);
		long t2 = System.currentTimeMillis();
		return (t2 - t1);
	}
	
	private long findBubbleSortTime(List<Integer> ip) {
		BubbleSort bs = new BubbleSort<Integer>();
		long t1 = System.currentTimeMillis();
		bs.sort(ip);
		long t2 = System.currentTimeMillis();
		return (t2 - t1);
	}
	
	private List<Integer> buildInput(int count) {
		List<Integer> op = new ArrayList();
		for (int i = count; i > 0; i--) {
			op.add(i);
		}
		return op;
	}

	static class SortInput {
		private String algorithm;
		private String sortInput;
		private String dataType;
		private boolean timeOnly;
		private boolean eliminateDuplicate;
		private int numberOfTrials;
		
		public int getNumberOfTrials() {
			return numberOfTrials;
		}
		public void setNumberOfTrials(int numberOfTrials) {
			this.numberOfTrials = numberOfTrials;
		}
		public boolean isEliminateDuplicate() {
			return eliminateDuplicate;
		}
		public void setEliminateDuplicate(boolean eliminateDuplicate) {
			this.eliminateDuplicate = eliminateDuplicate;
		}
		public boolean getTimeOnly() {
			return timeOnly;
		}
		public void setTimeOnly(boolean timeOnly) {
			this.timeOnly = timeOnly;
		}
		public String getDataType() {
			return dataType;
		}
		public void setDataType(String dataType) {
			this.dataType = dataType;
		}
		public String getAlgorithm() {
			return algorithm;
		}
		public void setAlgorithm(String algorithm) {
			this.algorithm = algorithm;
		}
		public String getSortInput() {
			return sortInput;
		}
		public void setSortInput(String sortInput) {
			this.sortInput = sortInput;
		}
		
		public String toString() {
			return "algorithm:" + algorithm + ";sortInput=" + sortInput + ";dataType=" + dataType + ";eliminateDuplicate=" + eliminateDuplicate + ";numberOfTrials=" + numberOfTrials;
		}
	}
}
